package com.project.form.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.form.dao.BookDao;
import com.project.form.model.Book;

@Service("bookService")
public class BookServiceImpl implements BookService {

	BookDao bookdao;

	@Autowired
	public void setBookDao(BookDao bookdao) {
		this.bookdao = bookdao;
	}

	@Override
	public Book findById(Integer id) {
		// TODO Auto-generated method stub
		return bookdao.findById(id);

	}

	@Override
	public List<Book> findAll() {
		// TODO Auto-generated method stub
		return bookdao.findAll();
	}

	@Override
	public void saveOrUpdate(Book book) {
		// TODO Auto-generated method stub
		if (findById(book.getId()) == null) {
			bookdao.save(book);
		} else {
			bookdao.update(book);
		}
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		bookdao.delete(id);
	}

}
