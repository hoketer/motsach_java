package com.project.form.service;

import java.util.List;

import com.project.form.model.Book;


public interface BookService {
	Book findById(Integer id);

	List<Book> findAll();

	void saveOrUpdate(Book book);

	void delete(int id);
}
