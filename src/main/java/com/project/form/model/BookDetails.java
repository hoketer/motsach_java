package com.project.form.model;

import java.util.Date;
import java.util.List;

public class BookDetails {
	Long Book_ID;
	String Book_name;
	Integer Author_ID;
	String Author_name;
	String Description;
	Date Published_date; 
	Integer Pages;
	String ISBN;
	String Lang;
	List<String> Tag;
	List<Integer> Category_ID;
	List<Integer> Review_ID;
	public Long getBook_ID() {
		return Book_ID;
	}
	public void setBook_ID(Long book_ID) {
		Book_ID = book_ID;
	}
	public String getBook_name() {
		return Book_name;
	}
	public void setBook_name(String book_name) {
		Book_name = book_name;
	}
	public Integer getAuthor_ID() {
		return Author_ID;
	}
	public void setAuthor_ID(Integer author_ID) {
		Author_ID = author_ID;
	}
	public String getAuthor_name() {
		return Author_name;
	}
	public void setAuthor_name(String author_name) {
		Author_name = author_name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public Date getPublished_date() {
		return Published_date;
	}
	public void setPublished_date(Date published_date) {
		Published_date = published_date;
	}
	public Integer getPages() {
		return Pages;
	}
	public void setPages(Integer pages) {
		Pages = pages;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public String getLang() {
		return Lang;
	}
	public void setLang(String lang) {
		Lang = lang;
	}
	public List<String> getTag() {
		return Tag;
	}
	public void setTag(List<String> tag) {
		Tag = tag;
	}
	public List<Integer> getCategory_ID() {
		return Category_ID;
	}
	public void setCategory_ID(List<Integer> category_ID) {
		Category_ID = category_ID;
	}
	public List<Integer> getReview_ID() {
		return Review_ID;
	}
	public void setReview_ID(List<Integer> review_ID) {
		Review_ID = review_ID;
	}
	@Override
	public String toString() {
		return "BookDetails [bookID=" + Book_ID + ", Book_name=" + Book_name + ", Author_ID=" + Author_ID
				+ ", Author_name=" + Author_name + ", Description=" + Description + ", Published_date=" + Published_date
				+ ", Pages=" + Pages + ", ISBN=" + ISBN + ", Lang=" + Lang + ", Tag=" + Tag + ", Category_ID="
				+ Category_ID + ", Review_ID=" + Review_ID + "]";
	}
	
}
