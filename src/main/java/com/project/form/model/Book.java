package com.project.form.model;

import java.util.List;

public class Book {
	Integer book_ID;

	String tensach;

	String tacgia;

	String nhanxet;

	String tinhtrang;

	List<String> theloai;
	
	public Integer getId() {
		return book_ID;
	}

	public void setId(Integer id) {
		this.book_ID = id;
	}

	public String getTensach() {
		return tensach;
	}

	public void setTensach(String tensach) {
		this.tensach = tensach;
	}

	public String getTacgia() {
		return tacgia;
	}

	public void setTacgia(String tacgia) {
		this.tacgia = tacgia;
	}

	public String getNhanxet() {
		return nhanxet;
	}

	public void setNhanxet(String nhanxet) {
		this.nhanxet = nhanxet;
	}

	public String getTinhtrang() {
		return tinhtrang;
	}

	public void setTinhtrang(String tinhtrang) {
		this.tinhtrang = tinhtrang;
	}

	public List<String> getTheloai() {
		return theloai;
	}

	public void setTheloai(List<String> theloai) {
		this.theloai = theloai;
	}
	
	public boolean isNew() 
	{
		return (this.book_ID ==null);
	}
	
	@Override
	public String toString() {
		return "Book [id=" + book_ID + ", tensach=" + tensach + ", tacgia=" + tacgia + ", nhanxet=" + nhanxet
				+ ", tinhtrang=" + tinhtrang + ", theloai=" + theloai + "]" + isNew();
	}

}
