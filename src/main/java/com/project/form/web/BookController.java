package com.project.form.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.form.model.Book;
import com.project.form.service.BookService;
import com.project.form.validator.BookFormValidator;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;

	@Autowired
	BookFormValidator bookFormValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(bookFormValidator);
	}

	@Autowired
	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "redirect:/motsach";
	}

	@RequestMapping(value = "/motsach", method = RequestMethod.GET)
	public String showAllBooks(Model model) {
		model.addAttribute("books", bookService.findAll());
		return "booksView/index";
	}

	@RequestMapping(value = "/motsach/add", method = RequestMethod.GET)
	public String showBookForm(Model model) {
		Book book = new Book();
		book.setTensach("Name of the book");
		book.setTacgia("The writer of the book");
		book.setNhanxet("What do you think about this book");
		book.setTinhtrang("describe your book. The look, the condition...");
		book.setTheloai(new ArrayList<String>(Arrays.asList("Horror", "Fantasy")));
		model.addAttribute("bookForm", book);
		populateDefaultModel(model);
		return "booksView/bookform";
	}
	
	@RequestMapping(value = "/motsach", method = RequestMethod.POST)
	public String saveBook(@ModelAttribute("bookForm") @Validated Book book,
			BindingResult result, Model model, final RedirectAttributes redirectAttributes)
	{

		if (result.hasErrors()) {
			populateDefaultModel(model);
			return "booksView/bookform";
		} else {

			redirectAttributes.addFlashAttribute("css", "success");
			if(book.isNew()){
				redirectAttributes.addFlashAttribute("msg", "book added successfully!");
			}else{
				redirectAttributes.addFlashAttribute("msg", "book updated successfully!");
			}
			
			bookService.saveOrUpdate(book);
			
			// POST/REDIRECT/GET
			return "redirect:/motsach/"; //+ book.getId();
		}
	}
	
	private void populateDefaultModel(Model model) 
	{
		List<String> genresList = new ArrayList<String>();
		genresList.add("Horror");
		genresList.add("Fantasy");
		genresList.add("Comedy");
		genresList.add("Romantic");
		model.addAttribute("genreList",genresList);
	}
}
