package com.project.form.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.project.form.model.Book;
import com.project.form.service.BookService;

@Component
public class BookFormValidator implements Validator {
	
	@Autowired
	BookService BookService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Book.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		Book book = (Book) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tensach", "NotEmpty.bookForm.tensach");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tacgia", "NotEmpty.bookForm.tacgia");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nhanxet", "NotEmpty.bookForm.nhanxet");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tinhtrang", "NotEmpty.bookForm.tinhtrang");
	
		if(book.getTheloai()==null){
			errors.rejectValue("theloai", "valid.bookFrom.theloai");
		}
	}

}
