package com.project.form.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.project.form.model.BookDetails;
import com.project.form.persistence.HibernateUtil;

public class BookDetailsDaoImpl {
	public static List<BookDetails> getBookDetails() {
		List<BookDetails> dsBookDetails;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		String hql = "from Bookdetails";
		Query query = session.createQuery(hql);
		dsBookDetails = query.list();
		return dsBookDetails;
	}

	public static void createBookDetails() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		BookDetails bookdetails = new BookDetails();
		bookdetails.setBook_name("1985");
		bookdetails.setAuthor_name("George Orwell");
		bookdetails.setDescription("blah blah");
		bookdetails.setPages(120);
	}

	public static void updateBookDetails() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		String hql = "update Bookdetails set Book_name := Bname, Author_name := Aname";
		Query query = session.createQuery(hql);
		query.setParameter("Bname", "1984");
		query.setParameter("Aname", "G. Orwell");
		int result = query.executeUpdate();
		session.getTransaction().commit();
	}

	public static void showBookDetailsList() {
		List<BookDetails> dsBookDetails = BookDetailsDaoImpl.getBookDetails();
		for (int i = 0; i < dsBookDetails.size(); i++) {
			System.out.println("Book Name:" +dsBookDetails.get(i).getBook_name()+"Author:" +"-"+ dsBookDetails.get(i).getAuthor_name());
		}

	}
}
