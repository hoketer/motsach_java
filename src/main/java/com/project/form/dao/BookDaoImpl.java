package com.project.form.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.project.form.model.Book;

import ch.qos.logback.core.net.SyslogOutputStream;

@Repository
public class BookDaoImpl implements BookDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws DataAccessException {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Book findById(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "SELECT * FROM books WHERE id=:id";

		Book result = null;
		try {
			result = namedParameterJdbcTemplate.queryForObject(sql, params, new BookMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}
		return result;
	}

	@Override
	public List<Book> findAll() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM books";
		List<Book> result = namedParameterJdbcTemplate.query(sql, new BookMapper());
		return result;
	}

	@Override
	public void save(Book book) {
		// TODO Auto-generated method stub
		KeyHolder keyHolder = new GeneratedKeyHolder();

		String sql = "INSERT INTO Books(TENSACH, TACGIA, NHANXET, TINHTRANG, THELOAI) "
				+ "VALUES ( :tensach, :tacgia, :nhanxet, :tinhtrang, :theloai)";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(book), keyHolder);
		book.setId(keyHolder.getKey().intValue());
	}

	@Override
	public void update(Book book) {
		// TODO Auto-generated method stub
		KeyHolder keyHolder = new GeneratedKeyHolder();

		String sql = "UPDATE BOOKS SET TENSACH=:tensach, TACGIA=:tacgia, NHANXET=:nhanxet," + "THELOAI=:theloai";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(book));
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM BOOKS WHERE Book_ID= :id";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private static final class BookMapper implements RowMapper<Book> {

		public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
			Book book = new Book();
			book.setId(rs.getInt("id"));
			book.setTensach(rs.getString("tensach"));
			book.setTacgia(rs.getString("tacgia"));
			book.setTheloai(convertDelimitedStringToList(rs.getString("theloai")));
			book.setTinhtrang(rs.getString("tinhtrang"));
			return book;
		}
	}

	private static List<String> convertDelimitedStringToList(String delimitedString) {

		List<String> result = new ArrayList<String>();

		if (!StringUtils.isEmpty(delimitedString)) {
			result = Arrays.asList(StringUtils.delimitedListToStringArray(delimitedString, ","));
		}
		return result;

	}

	private String convertListToDelimitedString(List<String> list) {

		String result = "";
		if (list != null) {
			result = StringUtils.arrayToCommaDelimitedString(list.toArray());
		}
		return result;

	}

	private SqlParameterSource getSqlParameterByModel(Book book) {

		// Unable to handle List<String> or Array
		// BeanPropertySqlParameterSource

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", book.getId());
		paramSource.addValue("tensach", book.getTensach());
		paramSource.addValue("tacgia", book.getTacgia());
		paramSource.addValue("nhanxet", book.getNhanxet());
		paramSource.addValue("tinhtrang", book.getTinhtrang());
		// join String
		paramSource.addValue("theloai", convertListToDelimitedString(book.getTheloai()));

		return paramSource;
	}

}
