package com.project.form.dao;

import java.util.List;

import com.project.form.model.Book;

public interface BookDao {
	
	Book findById(Integer id1);

	List<Book> findAll();

	void save(Book book);

	void update(Book book);

	void delete(Integer id);
}
