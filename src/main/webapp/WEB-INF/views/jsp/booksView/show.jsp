<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<div class="container">

	<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>${msg}</strong>
		</div>
	</c:if>

	<h1>Trade Post Detail</h1>
	<br />

	<div class="row">
		<label class="col-sm-2">ID</label>
		<div class="col-sm-10">${book.id}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Book's Name</label>
		<div class="col-sm-10">${book.tensach}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Author</label>
		<div class="col-sm-10">${book.tacgia}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Review</label>
		<div class="col-sm-10">${book.nhanxet}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Condition</label>
		<div class="col-sm-10">${book.tinhtrang}</div>
	</div>

	<div class="row">
		<label class="col-sm-2">Genre</label>
		<div class="col-sm-10">${user.thloai}</div>
	</div>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>