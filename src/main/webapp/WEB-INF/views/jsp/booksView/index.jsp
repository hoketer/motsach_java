<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>


<jsp:include page="../fragments/header.jsp" />

<body>


	<div class="container">

		<c:if test="${not empty msg}">
			<div class="alert alert-${css} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<strong>${msg}</strong>
			</div>
		</c:if>
		<div style="text-align: right; padding: 5px; margin: 5px 0px; background: #aaa;">
			<a href="${pageContext.request.contextPath}/motsach?lang=en">English</a>
			<a href="${pageContext.request.contextPath}/motsach?lang=vi">Vietnamese</a>
		</div>

		<h1>
			<spring:message code="label.tittle" />
		</h1>

		<table class="table table-striped">
			<thead>
				<tr>
					<th><spring:message code="label.ID" /></th>
					<th><spring:message code="label.name" /></th>
					<th><spring:message code="label.author" /></th>
					<th><spring:message code="label.genre" /></th>
					<th><spring:message code="label.action" /></th>
				</tr>
			</thead>

			<c:forEach var="book" items="${books}">
				<tr>
					<td>${book.id}</td>
					<td>${book.tensach}</td>
					<td>${book.tacgia}</td>
					<td><c:forEach var="theloai" items="${book.theloai}"
							varStatus="loop">
						${theloai}
    					<c:if test="${not loop.last}">,</c:if>
						</c:forEach></td>
				</tr>
			</c:forEach>
		</table>

	</div>

	<jsp:include page="../fragments/footer.jsp" />

</body>
</html>