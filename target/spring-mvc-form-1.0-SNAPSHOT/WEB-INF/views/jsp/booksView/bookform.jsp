<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<div class="container">
	<div style="text-align: right; padding: 5px; margin: 5px 0px; background: #321;">
			<a href="${pageContext.request.contextPath}/motsach/add?lang=en">English</a>
			<a href="${pageContext.request.contextPath}/motsach/add?lang=vi">Vietnamese</a>
	</div>
	
	<c:choose>
		<c:when test="${bookForm['new']}">
			<h1>Add Book</h1>
		</c:when>
		<c:otherwise>
			<h1>Update Book</h1>
		</c:otherwise>
	</c:choose>
	<br />

	<spring:url value="/motsach" var="bookActionUrl" />
	
	<form:form class="form-horizontal" method="post" modelAttribute="bookForm" action="${bookActionUrl}">
		<spring:bind path="tensach">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label"><th><spring:message code="label.name" /></th></label>
				<div class="col-sm-10">
					<form:input path="tensach" type="text" class="form-control " id="tensach" placeholder="Tensach" />
					<form:errors path="tensach" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="tacgia">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label"><th><spring:message code="label.author" /></th></label>
				<div class="col-sm-10">
					<form:input path="tacgia" class="form-control" id="tacgia" placeholder="tacgia" />
					<form:errors path="tacgia" class="control-label" />
				</div>
			</div>
		</spring:bind>
	
		<spring:bind path="tinhtrang">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label"><th><spring:message code="label.status" /></th></label>
				<div class="col-sm-10">
					<form:textarea path="tinhtrang" rows="3" class="form-control" id="tinhtrang" placeholder="tinhtrang" />
					<form:errors path="tinhtrang" class="control-label" />
				</div>
			</div>
		</spring:bind>
		
		<spring:bind path="nhanxet">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label"><th><spring:message code="label.review" /></th></label>
				<div class="col-sm-10">
					<form:textarea path="nhanxet" rows="3" class="form-control" id="nhanxet" placeholder="nhanxet" />
					<form:errors path="nhanxet" class="control-label" />
				</div>
			</div>
		</spring:bind>
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${bookForm['new']}">
						<button type="submit" class="btn-lg btn-primary pull-right">Add</button>
					</c:when>
					<c:otherwise>
						<button type="submit" class="btn-lg btn-primary pull-right">Update</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>